select * from dept;

-- select * from dept where id=1;

insert into dept(name, phone) values("技术部", "13100010001");

update dept set name="技术部", phone="13100010001" where id=1;

delete from dept where id=1;

CREATE TABLE `dept` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `phone` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话号码',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='科室表';