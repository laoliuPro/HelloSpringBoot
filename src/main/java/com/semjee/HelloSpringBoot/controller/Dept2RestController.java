package com.semjee.HelloSpringBoot.controller;

import com.github.pagehelper.PageInfo;
import com.semjee.HelloSpringBoot.entity.Dept;
import com.semjee.HelloSpringBoot.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// Restful风格
@RestController
@RequestMapping("v1/dept2")
public class Dept2RestController {

    @Autowired
    private DeptService deptService;

    // 分页
    @GetMapping(value = "")
    public PageInfo<Dept> getPage(){
        PageInfo<Dept> pageInfo = deptService.getPage(1, 3);
        return pageInfo;
    }

    // 所有数据
    @GetMapping(value = "list")
    public List<Dept> getAllDept(){
        return deptService.queryAllDept();
    }

    // 查看
    @GetMapping(value = "/{id}")
    public Dept queryDeptById(@PathVariable("id") Integer id){
        return deptService.queryDeptById(id);
    }

    // 修改
    @PutMapping(value = "")
    public Integer updateDept(Dept dept){
        return deptService.updateDept(dept);
    }

    // 新增
    @PostMapping(value = "")
    public Integer queryDeptById(Dept dept){
        return deptService.insertDept(dept);
    }

    // 删除
    @DeleteMapping(value = "/{id}")
    public Integer deleteDept(@PathVariable("id") Integer id){
        return deptService.deleteDept(id);
    }

}
