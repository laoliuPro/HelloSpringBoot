package com.semjee.HelloSpringBoot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "stu")
public class StudentController {

    // http://localhost:8080/stu/list
    @RequestMapping(value = "/list")
    public String ListInfo(){
        String str = "hello SpringBoot + Mybatis";
        System.out.println(str);
        return str;
    }

}
