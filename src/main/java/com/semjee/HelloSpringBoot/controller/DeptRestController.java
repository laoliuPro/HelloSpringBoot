package com.semjee.HelloSpringBoot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.semjee.HelloSpringBoot.entity.Dept;
import com.semjee.HelloSpringBoot.service.DeptService;
import com.semjee.HelloSpringBoot.unti.RespCode;
import com.semjee.HelloSpringBoot.unti.RespEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("dept")
public class DeptRestController {

    @Autowired
    private DeptService deptService;

    // 所有数据
    @GetMapping(value = "list")
    public List<Dept> getAllDept(){
        return deptService.queryAllDept();
    }

    // 所有数据
    @GetMapping(value = "test")
    public RespEntity getTestJson(){
        List<Dept> data = deptService.queryAllDept();
        return new RespEntity(RespCode.SUCCESS, data);
    }

    // 查看
    @GetMapping(value = "query/{id}")
    public RespEntity queryDeptById(@PathVariable("id") Integer id){
        RespEntity respEntity = new RespEntity();
        try {
            Dept data = deptService.queryDeptById(id);
            return respEntity.success("请求成功", data);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

    // 查看_bak_20231024
    @GetMapping(value = "query1/{id}")
    public RespEntity queryDeptById_bak_20231024(@PathVariable("id") Integer id){
        RespEntity respEntity = new RespEntity();
        try {
            Dept data = deptService.queryDeptById(id);
//            return new RespEntity(RespCode.SUCCESS, data);
//            return new RespEntity(200, "请求成功", data);
//            return new RespEntity.success(200, "请求成功", data);
//            return new RespEntity(RespCode(500, "请求成功!"), data);

            return respEntity.success("请求成功", data);
        } catch (Exception e) {
//            return new RespEntity(400, e.toString(), null);
            return respEntity.error(e.toString(), null);
        }
    }

    // 修改
    @PutMapping(value = "update")
    public RespEntity updateDept(Dept dept){
        RespEntity respEntity = new RespEntity();
        try {
            deptService.updateDept(dept);
            return respEntity.success("请求成功", null);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

    // 新增
    @PostMapping(value = "save")
    public RespEntity queryDeptById(Dept dept){
        RespEntity respEntity = new RespEntity();
        try {
            deptService.insertDept(dept);
            return respEntity.success("请求成功", null);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

    // 删除
    @DeleteMapping(value = "delete/{id}")
    public Integer deleteDept(@PathVariable("id") Integer id){
        return deptService.deleteDept(id);
    }

}
