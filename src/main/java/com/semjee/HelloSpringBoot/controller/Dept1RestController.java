package com.semjee.HelloSpringBoot.controller;

import com.github.pagehelper.PageInfo;
import com.semjee.HelloSpringBoot.entity.Dept;
import com.semjee.HelloSpringBoot.service.DeptService;
import com.semjee.HelloSpringBoot.unti.RespCode;
import com.semjee.HelloSpringBoot.unti.RespEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// Restful风格
@RestController
@RequestMapping("v1/dept")
public class Dept1RestController {

    @Autowired
    private DeptService deptService;

    // 分页
    @GetMapping(value = "")
    public RespEntity getPage(){
        PageInfo<Dept> pageInfo = deptService.getPage(1, 3);
        return new RespEntity(RespCode.SUCCESS, pageInfo);
    }

    // 所有数据
    @GetMapping(value = "list")
    public List<Dept> getAllDept(){
        return deptService.queryAllDept();
    }

    // 查看
    @GetMapping(value = "/{id}")
    public RespEntity queryDeptById(@PathVariable("id") Integer id){
        RespEntity respEntity = new RespEntity();
        try {
            Dept data = deptService.queryDeptById(id);
            return respEntity.success("请求成功", data);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

    // 修改
    @PutMapping(value = "")
    public RespEntity updateDept(Dept dept){
        RespEntity respEntity = new RespEntity();
        try {
            deptService.updateDept(dept);
            return respEntity.success("请求成功", null);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

    // 新增
    @PostMapping(value = "")
    public RespEntity queryDeptById(Dept dept){
        RespEntity respEntity = new RespEntity();
        try {
            deptService.insertDept(dept);
            return respEntity.success("请求成功", null);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

    // 删除
    @DeleteMapping(value = "/{id}")
    public RespEntity deleteDept(@PathVariable("id") Integer id){
        RespEntity respEntity = new RespEntity();
        try {
            deptService.deleteDept(id);
            return respEntity.success("请求成功", null);
        } catch (Exception e) {
            return respEntity.error(e.toString(), null);
        }
    }

}
