package com.semjee.HelloSpringBoot.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.semjee.HelloSpringBoot.entity.Dept;
import com.semjee.HelloSpringBoot.mapper.DeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService{

    @Autowired
    private DeptMapper deptMapper;

    /**
     * @param pageNum 当前页数
     * @param PageSize 每一页的数据量
     * @return
     */
    public PageInfo<Dept> getPage(Integer pageNum, Integer PageSize){
        PageHelper.startPage(pageNum, PageSize);
        List<Dept> list = deptMapper.queryAllDept();
        PageInfo<Dept> deptPageInfo = new PageInfo<>(list);
        return deptPageInfo;
    };

    /**
     * @return
     */
    @Override
    public List<Dept> queryAllDept() {
        return deptMapper.queryAllDept();
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Dept queryDeptById(Integer id) {
        return deptMapper.queryDeptById(id);
    }

    /**
     * @param dept dept对象
     * @return integer
     */
    @Override
    public Integer insertDept(Dept dept) {
        return deptMapper.insertDept(dept);
    }

    /**
     * @param dept
     * @return
     */
    @Override
    public Integer updateDept(Dept dept) {
        return deptMapper.updateDept(dept);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Integer deleteDept(Integer id) {
        return deptMapper.deleteDept(id);
    }
}
