package com.semjee.HelloSpringBoot.service;

import com.github.pagehelper.PageInfo;
import com.semjee.HelloSpringBoot.entity.Dept;

import java.util.List;

public interface DeptService {
    /**
     * @param pageNum 当前页数
     * @param PageSize 每一页的数据量
     * @return
     */
    PageInfo<Dept> getPage(Integer pageNum, Integer PageSize);
    List<Dept> queryAllDept();
    Dept queryDeptById(Integer id);
    Integer insertDept(Dept dept);
    Integer updateDept(Dept dept);
    Integer deleteDept(Integer id);
}
