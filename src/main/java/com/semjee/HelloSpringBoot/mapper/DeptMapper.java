package com.semjee.HelloSpringBoot.mapper;

import com.semjee.HelloSpringBoot.entity.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DeptMapper {

    @Select("select * from dept")
    List<Dept> queryAllDept();

    @Select("select * from dept where id=#{id}")
    Dept queryDeptById(Integer id);

    @Select("insert into dept(name, phone) values(#{name}, #{phone})")
    Integer insertDept(Dept dept);

    @Select("update dept set name=#{name}, phone=#{phone} where id=#{id}")
    Integer updateDept(Dept dept);

    @Select("delete from dept where id=#{id}")
    Integer deleteDept(Integer id);

}
