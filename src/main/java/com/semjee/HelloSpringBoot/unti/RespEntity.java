package com.semjee.HelloSpringBoot.unti;

public class RespEntity {
    private int code;
    private String msg;
    private Object data;

    public RespEntity() {
    }

    public RespEntity(RespCode respCode) {
        this.code = respCode.getCode();
        this.msg = respCode.getMsg();
    }

    public RespEntity(RespCode respCode, Object data) {
        this(respCode);
        this.data = data;
    }

    public RespEntity(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public RespEntity success(String msg, Object data) {
        this.code = 200;
        this.msg = msg;
        this.data = data;
        return this;
    }

    public RespEntity error(String msg, Object data) {
        this.code = 400;
        this.msg = msg;
        this.data = data;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}